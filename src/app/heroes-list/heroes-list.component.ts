import { Component, OnInit } from '@angular/core'

import { Hero } from '../hero'
import { HeroService } from '../hero.service'

@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.scss']
})
export class HeroesListComponent implements OnInit {
  heroes: Hero[]
  selectedHero: Hero

  constructor(private heroService: HeroService) {}

  ngOnInit() {
    this.getHeroes()
  }

  getHeroes(): void {
    this.heroService.getHeroes().subscribe(heroes => {
      this.heroes = heroes.sort((a, b) => a._id - b._id)
    })
  }

  selectHero(hero: Hero): void {
    this.selectedHero = this.selectedHero === hero ? null : hero
  }

  repopulateDb(): void {
    this.heroService.repopulateDb().subscribe(() => {
      this.getHeroes()
    })
  }
}
