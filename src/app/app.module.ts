import { NgModule } from '@angular/core'

import { BrowserModule } from '@angular/platform-browser'
import { AppRoutingModule } from './app-routing.module'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component'
import { HeroesListComponent } from './heroes-list/heroes-list.component'
import { HeroEditComponent } from './hero-edit/hero-edit.component'
import { HeroesDashboardComponent } from './heroes-dashboard/heroes-dashboard.component'

@NgModule({
  declarations: [AppComponent, HeroesListComponent, HeroEditComponent, HeroesDashboardComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
