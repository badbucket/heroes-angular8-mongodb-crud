import { Component, OnInit, Input } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'

import { Hero } from '../hero'
import { HeroService } from '../hero.service'

@Component({
  selector: 'app-hero-edit',
  templateUrl: './hero-edit.component.html',
  styleUrls: ['./hero-edit.component.scss']
})
export class HeroEditComponent implements OnInit {
  @Input() hero: Hero
  idParam: number

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) {}

  ngOnInit() {
    this.idParam = this.route.snapshot.params.id
    if (!this.idParam) {
      this.hero = new Hero()
      return
    }
    this.getHero(this.idParam)
  }

  goBack(): void {
    this.location.back()
  }

  getHero(id: number): void {
    this.heroService.getHero(id).subscribe(hero => (this.hero = hero))
  }

  updateHero(): void {
    const name = this.hero.name && this.hero.name.trim()
    if (!name) {
      return
    }
    if (this.idParam) {
      this.heroService.updateHero(this.hero).subscribe(() => this.goBack())
      return
    }
    this.heroService.addHero({ name } as Hero).subscribe(() => this.goBack())
  }

  deleteHero(): void {
    this.heroService.deleteHero(this.hero._id).subscribe(() => this.goBack())
  }
}
