import { NgModule } from '@angular/core'

import { RouterModule, Routes } from '@angular/router'

import { HeroesListComponent } from './heroes-list/heroes-list.component'
import { HeroEditComponent } from './hero-edit/hero-edit.component'
import { HeroesDashboardComponent } from './heroes-dashboard/heroes-dashboard.component'

const routes: Routes = [
  { path: '', redirectTo: '/heroes-dashboard', pathMatch: 'full' },
  { path: 'heroes-dashboard', component: HeroesDashboardComponent },
  { path: 'heroes-list', component: HeroesListComponent },
  { path: 'edit', component: HeroEditComponent },
  { path: 'edit/:id', component: HeroEditComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
