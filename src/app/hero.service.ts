import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Observable, of } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'

import { Hero } from './hero'
import { MOCK_HEROES } from './mock-heroes'

@Injectable({ providedIn: 'root' })
export class HeroService {
  apiUrl = '/api/'

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(private http: HttpClient) {}

  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.apiUrl).pipe(
      tap(_ => console.log('fetched Heroes')),
      catchError(this.handleError('getHeroes', []))
    )
  }

  getHero(id: number): Observable<Hero> {
    const url = `${this.apiUrl}${id}`
    return this.http.get<Hero>(url).pipe(
      tap(_ => console.log(`fetched Hero id=${id}`)),
      catchError(this.handleError<Hero>(`getHero id=${id}`))
    )
  }

  addHero(hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(this.apiUrl, hero, this.httpOptions).pipe(
      tap((h: Hero) => console.log(`added Hero w/ id=${h._id}`)),
      catchError(this.handleError<Hero>('addHero'))
    )
  }

  updateHero(hero: Hero): Observable<Hero> {
    const url = `${this.apiUrl}${hero._id}`
    return this.http.put(url, hero, this.httpOptions).pipe(
      tap((h: Hero) => console.log(`updated Hero id=${h._id}`)),
      catchError(this.handleError<any>('updateHero'))
    )
  }

  deleteHero(id: number): Observable<Hero> {
    const url = `${this.apiUrl}${id}`
    return this.http.delete<Hero>(url, this.httpOptions).pipe(
      tap(_ => console.log(`deleted Hero id=${id}`)),
      catchError(this.handleError<Hero>('deleteHero'))
    )
  }

  repopulateDb(): Observable<any> {
    const url = `${this.apiUrl}repopulate`
    return this.http.post<Hero>(url, MOCK_HEROES, this.httpOptions).pipe(
      tap(_ => console.log(`repopulated db`)),
      catchError(this.handleError<Hero>(`repopulateDb`))
    )
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error)
      return of(result as T)
    }
  }
}
