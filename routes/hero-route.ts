import { Request, Response, NextFunction } from 'express'
import Hero from '../models/hero'

export class HeroRoute {
  public heroRoute(app): void {
    app.route('/api/').get((req: Request, res: Response, next: NextFunction) => {
      Hero.find((err, heroes) => {
        if (err) {
          return next(err)
        }
        res.json(heroes)
      })
    })

    app.route('/api/:id').get((req: Request, res: Response, next: NextFunction) => {
      Hero.findById(req.params.id, (err, hero) => {
        if (err) {
          return next(err)
        }
        res.json(hero)
      })
    })

    app.route('/api/').post((req: Request, res: Response, next: NextFunction) => {
      Hero.create(req.body, (err, hero) => {
        if (err) {
          return next(err)
        }
        res.json(hero)
      })
    })

    app.route('/api/:id').put((req: Request, res: Response, next: NextFunction) => {
      Hero.findByIdAndUpdate(req.params.id, req.body, (err, hero) => {
        if (err) {
          return next(err)
        }
        res.json(hero)
      })
    })

    app.route('/api/:id').delete((req: Request, res: Response, next: NextFunction) => {
      Hero.findByIdAndRemove(req.params.id, req.body, (err, hero) => {
        if (err) {
          return next(err)
        }
        res.json(hero)
      })
    })

    app.route('/api/repopulate').post((req: Request, res: Response, next: NextFunction) => {
      Hero.deleteMany({})
        .then(() => {
          console.log('db emptied')
          Hero.resetCount(() => {
            Hero.create(req.body)
              .then(data => {
                console.log('dummy entries inserted to db ', data)
                res.json(data)
              })
              .catch(err => next(err))
          })
        })
        .catch(err => next(err))
    })
  }
}
