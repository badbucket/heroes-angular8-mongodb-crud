import mongoose, { Schema } from 'mongoose'

const connection = mongoose.createConnection('mongodb://localhost/angular8-mongodb-crud')

const autoIncrement = require('mongoose-auto-increment')
autoIncrement.initialize(connection)

const HeroSchema: Schema = new Schema({
  name: { type: String, required: true }
})

HeroSchema.plugin(autoIncrement.plugin, { model: 'Hero', startAt: 11 })

export default connection.model('Hero', HeroSchema)
